

export const TOGGLE_ANDROID_MODAL = "TOGGLE_ANDROID_MODAL";
export const TOGGLE_IOS_MODAL = "TOGGLE_IOS_MODAL";

export function toggleAndroidModal(isOpen){
    return{
        type:TOGGLE_ANDROID_MODAL,
        isOpen,
    }
}
export function toggleIOSModal(isOpen){
    return{
        type:TOGGLE_IOS_MODAL,
        isOpen,
    }
}