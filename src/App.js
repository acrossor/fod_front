import React, { Component } from 'react';
import {styledTheme} from './components/styles'
import {ThemeProvider} from 'styled-components'
import Nav from './components/nav/index'
import Scroller from './components/scroll/index'
import {AndroidDownloadModal,IOSDownloadModal} from './components/modals/downloadModal'
import { Provider } from 'react-redux'
import { createStore } from 'redux'
import reducers from './reducers'


const store = createStore(reducers);

class App extends Component {
    render() {
        return (
            <Provider store={store}>
                <ThemeProvider theme={styledTheme}>
                    <div>
                        <Nav/>
                        <Scroller/>
                        <AndroidDownloadModal/>
                        <IOSDownloadModal/>
                    </div>
                </ThemeProvider>
            </Provider>
        );
    }
}

export default App;
