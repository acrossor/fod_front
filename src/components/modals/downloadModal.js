import {Component} from "react";
import Modal from "@material-ui/core/Modal/Modal";
import React from "react";
import styled from "styled-components";
import { connect } from 'react-redux'
import {toggleAndroidModal,toggleIOSModal} from '../../actions/modalActions'
import {withTheme} from 'styled-components'

import androidQR1 from '../../images/modal/androidQR1.svg'
import androidQR2 from '../../images/modal/androidQR2.svg'
import googlePlayBadge from '../../images/modal/google-play-badge.svg'
import modalTitleIcon from '../../images/modal/modalTitleIcon.svg'
import appleStoreBadge from '../../images/modal/appleStoreBadge.svg'
import iOSQR from '../../images/modal/ios_qr.svg'


const ModalContainer = styled.div`
  top:50%;
  left:50%;
  transform:translate(-50%,-50%);
  width:500px;
  position:absolute;
  border-radius: 30px;
  padding:${props=>props.theme.spacing.unit*6}px;
  background:white;
  box-shadow: 0 0 10px black;
  outline:none;
  display:flex;
  flex-direction: column;
`;

const FlexRowDiv = styled.div`
  display:flex;
  flex-direction: row;
`;

const FlexCenterItem = styled.div`
  flex:1;
  display:flex;
  justify-content: center;
  align-items: center;
`;

class AndroidModal extends Component {
    render(){
        const {theme, closeModal,isOpen} = this.props;
        const spaceUnit = theme.spacing.unit;
        const primary = theme.palette.text.primary;
        const secondary = theme.palette.text.secondary;
        return(

            <Modal
                open={isOpen}
                onClose={closeModal}
            >
                <ModalContainer>
                    <FlexRowDiv>
                        <div style={{flex:0,marginRight:spaceUnit*4, marginBottom:spaceUnit*4}}>
                            <img src={modalTitleIcon} alt=" "/>
                        </div>
                        <div style={{flex:1,color:primary,fontSize:16,fontWeight:'bold'}}>
                            <p style={{fontSize:16,marginTop:0}}>FOD - Food On Delivery</p>
                            <p style={{fontSize:14}}>FOD  Food & Drink</p>
                        </div>
                    </FlexRowDiv>

                    <div style={{lineHeight:'1em',marginBottom:spaceUnit*4}}>
                        F.O.D. is an essential application which is focusing on maximizing the user experience both online and offline regarding food order and delivery. Browse tens of thousands menus of your favourite restaurants across GTA area. And we are continuing to expand and serve for all the Canadian from coast to coast.
                    </div>

                    <FlexRowDiv style={{marginBottom:spaceUnit*4,justifyContent:'space-around',color:secondary,fontSize:16,overflow:'hidden',whiteSpace:'nowrap'}}>
                        <div style={{flex:0}}>
                            <span style={{display:'block'}}><b>GOOGLE PLAY</b></span>
                            <span style={{display:'block',marginBottom:spaceUnit}}>QR CODE</span>
                            <img src={androidQR1} alt=" "/>
                        </div>
                        <div style={{flex:0}}>
                            <span style={{display:'block'}}><b>APK DOWNLOAD</b></span>
                            <span style={{display:'block',marginBottom:spaceUnit}}>QR CODE</span>
                            <img src={androidQR2} alt=" "/>
                        </div>
                    </FlexRowDiv>

                    <FlexRowDiv style={{margin:`${spaceUnit*2}px 0`}}>
                        <FlexCenterItem as={'a'} href="https://play.google.com/store/apps/details?id=com.fod.fodwebwrap" rel="noopener noreferrer" target='_blank'>
                            <img src={googlePlayBadge} alt={' '}/>
                        </FlexCenterItem>
                        <FlexCenterItem style={{color:secondary,fontSize:16}}>
                            <span >DESIGN FOR <b>ANDROID</b></span>
                        </FlexCenterItem>

                    </FlexRowDiv>
                </ModalContainer>
            </Modal>
        )
    }
}

const mapStateToPropsAndroid = state => {
    return {
        isOpen: state.modal.androidOpen,
    }
};

const mapDispatchToPropsAndroid = dispatch => {
    return {
        closeModal: () => {
            dispatch(toggleAndroidModal(false));
        },
    }
};

export const AndroidDownloadModal = connect(
    mapStateToPropsAndroid,
    mapDispatchToPropsAndroid
)(withTheme(AndroidModal));


class IOSModal extends Component {
    render(){
        const {theme, closeModal,isOpen} = this.props;
        const spaceUnit = theme.spacing.unit;
        const primary = theme.palette.text.primary;
        const secondary = theme.palette.text.secondary;
        return(

            <Modal
                open={isOpen}
                onClose={closeModal}
            >
                <ModalContainer>
                    <FlexRowDiv>
                        <div style={{flex:0,marginRight:spaceUnit*4, marginBottom:spaceUnit*4}}>
                            <img style={{borderRadius:20}} src={modalTitleIcon} alt=" "/>
                        </div>
                        <div style={{flex:1,color:primary,fontSize:16,fontWeight:'bold'}}>
                            <p style={{fontSize:16,marginTop:0}}>FOD - 订餐配送平台</p>
                            <p style={{fontSize:14}}>Fast Quality Professional<br/>Food On Delivery</p>
                        </div>
                    </FlexRowDiv>

                    <div style={{lineHeight:'1em',marginBottom:spaceUnit*4}}>
                        F.O.D. is an essential application which is focusing on maximizing the user experience both online and offline regarding food order and delivery. Browse tens of thousands menus of your favourite restaurants across GTA area. And we are continuing to expand and serve for all the Canadian from coast to coast.
                    </div>

                    <FlexRowDiv style={{marginBottom:spaceUnit*4,color:secondary,fontSize:16,overflow:'hidden',whiteSpace:'nowrap'}}>
                        <FlexCenterItem>
                            <span>Scan <b>QR CODE</b></span>
                        </FlexCenterItem>
                        <FlexCenterItem>
                            <img src={iOSQR} alt=" "/>
                        </FlexCenterItem>

                        <FlexCenterItem/>
                    </FlexRowDiv>

                    <FlexRowDiv style={{margin:`${spaceUnit*2}px 0`}}>
                        <FlexCenterItem/>
                        <FlexCenterItem as={'a'} href="https://itunes.apple.com/ca/app/fod-%E8%AE%A2%E9%A4%90%E9%85%8D%E9%80%81%E5%B9%B3%E5%8F%B0/id1280702579" rel="noopener noreferrer" target='_blank'>
                            <img src={appleStoreBadge} alt={' '}/>
                        </FlexCenterItem>
                        <FlexCenterItem style={{color:secondary,fontSize:16}}>
                            <span >DESIGN FOR <b>IOS</b></span>
                        </FlexCenterItem>

                    </FlexRowDiv>
                </ModalContainer>
            </Modal>
        )
    }
}

const mapStateToPropsIOS = state => {
    return {
        isOpen: state.modal.iOSOpen,
    }
};

const mapDispatchToPropsIOS = dispatch => {
    return {
        closeModal: () => {
            dispatch(toggleIOSModal(false));
        },
    }
};

export const IOSDownloadModal = connect(
    mapStateToPropsIOS,
    mapDispatchToPropsIOS
)(withTheme(IOSModal));



