import React, {Component} from 'react'
import styled from 'styled-components'
import logo from '../../images/nav/fodIcon.svg'
import {toggleAndroidModal, toggleIOSModal} from "../../actions/modalActions";
import connect from "react-redux/es/connect/connect";
const Nav = styled.div.attrs(props=>({
    paddingTop:props.theme.spacing.unit*8,
    paddingBottom:props.theme.spacing.unit*8,
    height:props.theme.dim.navHeight-props.theme.spacing.unit*(8+8),
}))`
    padding-top:${props=>props.paddingTop}px;
    padding-bottom:${props=>props.paddingBottom}px;
    height:${props=>props.height}px;
    
    padding-left:${props=>props.theme.spacing.unit*10}px;
    padding-right:${props=>props.theme.spacing.unit*10}px;
    display:flex;
    width:100%;
    box-sizing: border-box;
    top:0;
    
`;

//resolve issue for collapse padding/margin
const NavContainer = styled.div`
top:0;
    position:fixed;
  width: 100%;
  height: ${props=>props.theme.dim.navHeight}px;
  background-color:white;
    z-index: 100;

`;

const TitleIcon = styled.div`
    color:${props=>props.theme.palette.text.primary};
    height:50px;
    width:100px;
`;

export const ButtonSmall = styled.div`
  padding: ${props=>props.theme.spacing.unit}px ${props=>props.theme.spacing.unit*3}px;
  color:${props=>props.theme.palette.text.primary};
  font-size:16px;
  font-weight:bold;
  margin: auto ${props=>props.theme.spacing.unit*2}px;
  border:2px solid ${props=>props.theme.palette.text.secondary};
  border-radius:${props=>props.theme.spacing.unit*4}px;
  cursor:pointer;
    vertical-align: middle;
  
  :hover{
    color:white;
    background-color:${props=>props.theme.palette.text.primary};
    border-color:${props=>props.theme.palette.text.primary};
  }
`;

const OnDemand = styled.div`
  position:relative;
  width:${props=>props.theme.spacing.unit*20}px;
  text-align: right;
  display:flex;
`;

const OnDemandCircle = styled.div.attrs(props=>({
    dim:props.theme.spacing.unit*20,

}))`
    height:${props=>props.dim}px;
    width:${props=>props.dim}px;
    background-color:${props=>props.theme.palette.main};
    border-radius:${props=>props.dim}px;
    top:-${props=>props.theme.spacing.unit*9}px;
    position:absolute;
    z-index:0;
`;
const OnDemandText = styled.span`
position:relative;
    padding:${props=>props.theme.spacing.unit}px 0;
    margin-right:${props=>props.theme.spacing.unit*2}px;
  color:white;
  font-size:14px;
  font-weight:bold;
  z-index:1;
  width:100%;
 `;

class NavigatorBar extends Component{
    constructor(props){
        super(props);
        this.state={
            drawerOpen:false,
        };
    }


    render(){
        return(
            <NavContainer>
                <Nav>
                    <TitleIcon style={{flex:"1 0"}}>
                        <img alt={'title'} src={logo}/>
                    </TitleIcon>
                    <ButtonSmall onClick={this.props.openAndroidModal}>
                        ANDROID APP
                    </ButtonSmall>
                    <ButtonSmall onClick={this.props.openIOSModal}>
                        IOS APP
                    </ButtonSmall>
                    <OnDemand >
                        <OnDemandText>OnDemand</OnDemandText>
                        <OnDemandCircle/>
                    </OnDemand>
                </Nav>
            </NavContainer>
        )
    }
}


const mapDispatchToProps = dispatch => {
    return {
        openAndroidModal: () => {
            dispatch(toggleAndroidModal(true));
        },
        openIOSModal: () => {
            dispatch(toggleIOSModal(true));
        },
    }
};

export default connect(
    null,
    mapDispatchToProps
)(NavigatorBar);
