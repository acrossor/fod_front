
import React, {Component} from 'react'
import {connect} from 'react-redux'
import {ScrollFrame, ScrollItem, Section, ANIITEM_CONST} from '../../library/animationScroll'
import styled,{withTheme} from 'styled-components';
import {ButtonSmall} from '../nav/index'
import Drawer from "@material-ui/core/Drawer/Drawer";
import HighlightOffIcon from '@material-ui/icons/HighlightOff';
import iOSBG from '../../images/secIOS/bg.png' //0.6:1
import iOSPhone from '../../images/secIOS/phone.png'
import iOSFrame from '../../images/secIOS/i1.png'
import androidBG from '../../images/secAndroid/bg.png' //0.6:1
import androidPhone from '../../images/secAndroid/phone.png'
import detailImg1 from '../../images/secDetail/img1.png'
import detailImg2 from '../../images/secDetail/img2.png'
import detailImg3 from '../../images/secDetail/img3.png'
import bgIcon from '../../images/nav/fod-bg-icon.svg'
import {toggleIOSModal,toggleAndroidModal} from '../../actions/modalActions'

const phoneRatio = 390/800;
const aspectRatio = 16/9.75;

//const androidRatio = 390/804;
//const androidExpandRatio = 421/814;


const fodBgRatio = 623/511;

const detailRatio = 720/381;


const TandC = styled.span`
  cursor:pointer;
  
  :hover{
    color:${props=>props.theme.palette.text.primary};
    font-weight: bold;
  }
`;

const DrawerSide = styled.div`
  height: calc(100vh - ${props=>props.theme.dim.navHeight}px);
  padding-top:${props=>props.theme.spacing.unit*5}px;
  padding-left:${props=>props.theme.spacing.unit*3}px;
  width:${props=>props.theme.spacing.unit*(10+10)+159}px;
  position:absolute;
  bottom:0;
  background:white;
  font-weight: bold;
  font-size:20px;
  color:${props=>props.theme.palette.text.primary};
  box-sizing: border-box;
`;

const DrawerSideItem = styled.div`
  border-bottom: 3px solid ${props=>props.theme.palette.text.primary};
  width:100%;
  cursor:pointer;
  z-index: 10;
`;



class Scroller extends Component{
    constructor(props){
        super(props);
        this.state = {drawerOpen:false,androidOpen:false,iOSOpen:false};
    }
    renderiOSSection=()=>{//TODO refactor calculation to outside
        const {theme} = this.props;
        let bgWidth = 86;
        let bgVer = (100-bgWidth*0.6*aspectRatio)/2;
        let bgTextHor = 10;
        //*0.6 /aspectRatio

        let phoneVer = (bgVer-2.2);
        let phoneHeight = 100-(phoneVer*2);
        let phoneWidth = phoneHeight*phoneRatio/aspectRatio;
        let phoneLeft = 18;
        let phoneVerShift = 1.0;

        let textRight = 10;

        const actL = 200;

        const wordToFrameRest = 100;
        const phoneToRollRest = 50;
        const endSpace = 200;

        const wordEnd= 100;
        const frameStart = wordEnd+wordToFrameRest+actL*2;
        const phoneStart = frameStart+actL;
        const bgEnd = phoneStart;
        const rollStart = phoneStart + phoneToRollRest + actL*2;
        const contentHeight = rollStart+endSpace;

        return(
            <Section contentHeight={contentHeight}>
                <ScrollItem as={'div'} itemStyle={{background:`url(${iOSBG}) no-repeat left center`, backgroundSize:'contain',
                    top:bgVer+'%',bottom:bgVer+'%',left:'0',right:(100-bgWidth)+'%'}}
                            start={0} end={bgEnd}
                            outMethod={ANIITEM_CONST.getValidMethod(ANIITEM_CONST.METHOD.FADE,null)}
                />

                <ScrollItem as={'div'} itemStyle={{fontSize:72,bottom:`${60}%`,left:`${bgTextHor}%`,color:'white',fontFamily:'Gochi Hand'}}
                            start={0} end={wordEnd}
                            outMethod={ANIITEM_CONST.getValidMethod(ANIITEM_CONST.METHOD.FADE,null)}>
                    Food, Drink & Grocery
                </ScrollItem>
                <ScrollItem as={'div'} itemStyle={{fontSize:72,bottom:`${50}%`,right:`${bgTextHor+(100-bgWidth)}%`,color:'white',fontFamily:'Gochi Hand'}}
                            start={0} end={wordEnd}
                            outMethod={ANIITEM_CONST.getValidMethod(ANIITEM_CONST.METHOD.FADE,null)}>
                    We Deliver More
                </ScrollItem>

                <ScrollItem as={'div'} itemStyle={{background:`url(${iOSFrame}) no-repeat  center`, backgroundSize:'contain',
                    top:`${phoneVer+phoneVerShift}%`,bottom:`${phoneVer-phoneVerShift}%`,left:`${phoneLeft}%`,right:`${100- (phoneLeft+phoneWidth)}%`}}
                            start={frameStart} end={frameStart+199}
                            inMethod={ANIITEM_CONST.getValidMethod(ANIITEM_CONST.METHOD.FADE,null)}
                            outMethod={ANIITEM_CONST.getValidMethod(ANIITEM_CONST.METHOD.SUDDEN,null)}
                />
                <ScrollItem as={'div'} itemStyle={{background:`url(${iOSPhone}) no-repeat  center`, backgroundSize:'contain',
                    top:`${phoneVer+phoneVerShift}%`,bottom:`${phoneVer-phoneVerShift}%`,left:`${phoneLeft}%`,right:`${100- (phoneLeft+phoneWidth)}%`}}
                            start={phoneStart} end={0}
                            inMethod={ANIITEM_CONST.getValidMethod(ANIITEM_CONST.METHOD.SUDDEN,null)}
                />

                <ScrollItem as={'div'} itemStyle={{fontSize:72, fontWeight:'bold', bottom:`${65}%`,right:`${textRight}%`,color:theme.palette.text.secondary}}
                            start={rollStart} end={0}
                            inMethod={ANIITEM_CONST.getValidMethod(ANIITEM_CONST.METHOD.ROLL,ANIITEM_CONST.DIRECTION.UP)}>
                    Get The App
                </ScrollItem>
                <ScrollItem as={'div'} itemStyle={{fontSize:72, fontWeight:'bold',letterSpacing:-3, bottom:`${50}%`,right:`${textRight}%`,color:theme.palette.text.primary}}
                            start={rollStart} end={0}
                            inMethod={ANIITEM_CONST.getValidMethod(ANIITEM_CONST.METHOD.ROLL,ANIITEM_CONST.DIRECTION.UP)}>
                    F.O.D OnDemand
                </ScrollItem>
                <ScrollItem as={'div'} itemStyle={{fontSize:48,bottom:`${38}%`,right:`${textRight}%`,color:theme.palette.text.primary}}
                            start={rollStart} end={0}
                            inMethod={ANIITEM_CONST.getValidMethod(ANIITEM_CONST.METHOD.ROLL,ANIITEM_CONST.DIRECTION.UP)}>
                    For Apple <b>IOS</b>
                </ScrollItem>
                <ScrollItem as={'div'} itemStyle={{fontSize:16,bottom:`${20}%`,right:`${textRight}%`,color:theme.palette.text.primary}}
                            start={rollStart} end={0}
                            inMethod={ANIITEM_CONST.getValidMethod(ANIITEM_CONST.METHOD.ROLL,ANIITEM_CONST.DIRECTION.UP)}>
                    <ButtonSmall onClick={this.props.openIOSModal}>
                        Download
                    </ButtonSmall>
                </ScrollItem>
            </Section>
        )
    };

    renderAndroidSection=()=>{ //TODO refactor calculation to outside
        const {theme} = this.props;
        let bgWidth = 86;
        let bgVer = (100-bgWidth*0.6*aspectRatio)/2;
        let bgTextHor = 5;
        //*0.6 /aspectRatio

        let phoneVer = (bgVer-5);
        let phoneHeight = 100-(phoneVer*2);
        let phoneWidth = phoneHeight*phoneRatio/aspectRatio;
        let phoneActionWidth = phoneWidth*814/804;
        let phoneActionHeight = phoneHeight*421/390;
        let phoneActualVer = (100-phoneActionHeight)/2;


        let phoneRight = 26.4;
        let phoneVerShift = 1.3;

        let textLeft = 10;



        const actL = 200;
        const wordToPhoneRest = 100;
        const phoneToRollRest = 50;
        const endSpace = 200;

        const wordStart = 300;
        const wordEnd= wordStart+actL*2;
        const phoneStart = wordEnd+wordToPhoneRest+actL*2;
        const bgEnd = phoneStart;
        const rollStart = phoneStart + phoneToRollRest + actL*2;
        const contentHeight = rollStart+endSpace;


        return(
            <Section contentHeight={contentHeight}>
                <ScrollItem as={'div'} itemStyle={{background:`url(${androidBG}) no-repeat left center`, backgroundSize:'contain',
                    top:bgVer+'%',bottom:bgVer+'%',right:'0',left:(100-bgWidth)+'%'}}
                            start={0} end={bgEnd}
                            outMethod={ANIITEM_CONST.getValidMethod(ANIITEM_CONST.METHOD.FADE,null)}
                />

                <ScrollItem as={'div'} itemStyle={{fontSize:64,bottom:`${60}%`,left:`${(100-bgWidth)+bgTextHor}%`,color:'white',fontFamily:'Gochi Hand'}}
                            start={wordStart} end={wordEnd}
                            inMethod={ANIITEM_CONST.getValidMethod(ANIITEM_CONST.METHOD.FADE,null)}
                            outMethod={ANIITEM_CONST.getValidMethod(ANIITEM_CONST.METHOD.FADE,null)}>
                    We Redefine the meaning
                </ScrollItem>
                <ScrollItem as={'div'} itemStyle={{fontSize:64,bottom:`${50}%`,left:`${(100-bgWidth)+bgTextHor}%`,color:'white',fontFamily:'Gochi Hand'}}
                            start={wordStart} end={wordEnd}
                            inMethod={ANIITEM_CONST.getValidMethod(ANIITEM_CONST.METHOD.FADE,null)}
                            outMethod={ANIITEM_CONST.getValidMethod(ANIITEM_CONST.METHOD.FADE,null)}>
                    of Delivery
                </ScrollItem>

                <ScrollItem as={'div'} itemStyle={{background:`url(${androidPhone}) no-repeat  center`, backgroundSize:'contain',
                    top:`${phoneActualVer+phoneVerShift}%`,bottom:`${phoneActualVer-phoneVerShift}%`,right:`${phoneRight}%`,left:`${100- (phoneRight+phoneActionWidth)}%`}}
                            start={phoneStart} end={0}
                            inMethod={ANIITEM_CONST.getValidMethod(ANIITEM_CONST.METHOD.FADE,null,400)}
                />

                <ScrollItem as={'div'} itemStyle={{fontSize:72, fontWeight:'bold' ,bottom:`${50}%`,left:`${textLeft}%`,color:theme.palette.text.primary}}
                            start={rollStart} end={0}
                            inMethod={ANIITEM_CONST.getValidMethod(ANIITEM_CONST.METHOD.ROLL,ANIITEM_CONST.DIRECTION.UP)}>
                    New Design
                </ScrollItem>
                <ScrollItem as={'div'} itemStyle={{fontSize:48,bottom:`${38}%`,left:`${textLeft}%`,color:theme.palette.text.primary}}
                            start={rollStart} end={0}
                            inMethod={ANIITEM_CONST.getValidMethod(ANIITEM_CONST.METHOD.ROLL,ANIITEM_CONST.DIRECTION.UP)}>
                    For <b>Android</b>
                </ScrollItem>
                <ScrollItem as={'div'} itemStyle={{fontSize:16,bottom:`${20}%`,left:`${textLeft}%`,color:theme.palette.text.primary}}
                            start={rollStart} end={0}
                            inMethod={ANIITEM_CONST.getValidMethod(ANIITEM_CONST.METHOD.ROLL,ANIITEM_CONST.DIRECTION.UP)}>
                    <ButtonSmall onClick={this.props.openAndroidModal}>
                        Download
                    </ButtonSmall>
                </ScrollItem>
            </Section>
        )
    };


    renderDetailSection=()=>{ //TODO refactor calculation to outside
        const {theme} = this.props;
        let bgWidth = 100;
        let bgVer = (100-(bgWidth/2/detailRatio)*2*aspectRatio)/2; //two horizontal,  from hor=>ver so /AR

        let circleDiameter = (100-(bgVer+3)*2);
        let circleRadiusSegHor = circleDiameter/2/5;
        let circleRadiusSegVer = circleRadiusSegHor/aspectRatio;
        let circleRadiusSegHorExtend = circleRadiusSegHor+1;
        let circleRadiusSegVerExtend = circleRadiusSegHorExtend/aspectRatio;

        let bgTextTop= 5;
        let bgTextHor = circleRadiusSegHor*0.3;
        let bgTextVer = circleRadiusSegVer*0.3;
        let textMore = bgTextTop*2;


        const actL = 200;
        const roll1ToBg = 200;
        const bgToRoll2 = 200;
        const endSpace = 200;

        const roll1Start = 100+ actL;
        const bgStart = roll1Start + actL+roll1ToBg;
        const roll2Start = bgStart + actL+bgToRoll2;
        const contentHeight = roll2Start+endSpace;

        return(
            <Section contentHeight={contentHeight}>
                <ScrollItem as={'div'} itemStyle={{background:`url(${detailImg1}) no-repeat center`, backgroundSize:'contain',
                    top:`${bgVer}%`,bottom:`${50}%`,right:`${50}%`,left:`0`}}
                            start={0} end={0}
                />

                <ScrollItem as={'div'} itemStyle={{background:`url(${detailImg2}) no-repeat center`, backgroundSize:'contain',
                    top:`${bgVer}%`,bottom:`${50}%`,left:`${50}%`,right:`0`}}
                            start={bgStart} end={0}
                            inMethod={ANIITEM_CONST.getValidMethod(ANIITEM_CONST.METHOD.FADE,null)}
                />

                <ScrollItem as={'div'} itemStyle={{background:`url(${detailImg3}) no-repeat center`, backgroundSize:'contain',
                    top:`${50}%`,bottom:`${bgVer}%`,right:`${50}%`,left:`0`}}
                            start={bgStart} end={0}
                            inMethod={ANIITEM_CONST.getValidMethod(ANIITEM_CONST.METHOD.FADE,null)}
                />


                <ScrollItem as={'div'} itemStyle={{backgroundColor:'rgba(170,170,170,0.33)', borderRadius:2000,
                    top:`${50-circleRadiusSegHor*5}%`,bottom:`${50-circleRadiusSegHor*5}%`,right:`${50-circleRadiusSegVer*5}%`,left:`${50-circleRadiusSegVer*5}%`}}
                            start={0} end={0}
                />

                <ScrollItem as={'div'} itemStyle={{fontSize:144 ,bottom:`${50}%`,left:`${50+bgTextHor}%`,color:'white',fontWeight:'bold',lineHeight:'0.85em'}}
                            start={bgStart} end={0}
                            inMethod={ANIITEM_CONST.getValidMethod(ANIITEM_CONST.METHOD.FADE)}>
                    DRINK
                </ScrollItem>

                <ScrollItem as={'div'} itemStyle={{fontSize:144 ,top:`${50+bgTextVer}%`,right:`${50+bgTextHor}%`,color:'white',fontWeight:'bold',lineHeight:'0.85em'}}
                            start={bgStart} end={0}
                            inMethod={ANIITEM_CONST.getValidMethod(ANIITEM_CONST.METHOD.FADE)}>
                    GROCERY
                </ScrollItem>

                <ScrollItem as={'div'} itemStyle={{fontSize:144 ,bottom:`${50}%`,right:`${50+bgTextHor}%`,color:'white',fontWeight:'bold',lineHeight:'0.85em'}}
                            start={0} end={0}>
                    FOOD
                </ScrollItem>

                <ScrollItem as={'div'} itemStyle={{backgroundColor:'rgba(255,255,255,0.33)', borderRadius:2000,
                    top:`${50-circleRadiusSegHor*3}%`,bottom:`${50-circleRadiusSegHor*3}%`,right:`${50-circleRadiusSegVer*3}%`,left:`${50-circleRadiusSegVer*3}%`}}
                            start={roll2Start} end={0}
                            inMethod={ANIITEM_CONST.getValidMethod(ANIITEM_CONST.METHOD.FADE,null)}
                />


                <ScrollItem as={'div'} itemStyle={{backgroundColor:'rgba(255,255,255,1)', borderRadius:2000,
                    top:`${50-circleRadiusSegHorExtend}%`,bottom:`${50-circleRadiusSegHorExtend}%`,right:`${50-circleRadiusSegVerExtend}%`,left:`${50-circleRadiusSegVerExtend}%`}}
                            start={0} end={0}
                />

                <ScrollItem as={'div'} itemStyle={{fontSize:120, fontWeight:'900',fontStretch:'condensed',lineHeight:'0.8em',color:theme.palette.main,
                    top:`${50}%`,left:`${50}%`, transform:'translate(-50%,-55%)'}}
                            start={roll2Start} end={0}
                            inMethod={ANIITEM_CONST.getValidMethod(ANIITEM_CONST.METHOD.FADE)}>
                    U
                </ScrollItem>

                <ScrollItem as={'div'} itemStyle={{fontSize:18 ,top:`${50+bgTextTop}%`,left:`${50+bgTextTop}%`, right:`${bgTextTop}%`, textAlign:'left',color:theme.palette.text.primary}}
                            start={roll1Start} end={0}
                            inMethod={ANIITEM_CONST.getValidMethod(ANIITEM_CONST.METHOD.ROLL,ANIITEM_CONST.DIRECTION.UP)}>
                    Get groceries and alcohol delivered in under an hour so you can spend your time living your best life. Whether you need a gallon of milk or a handle of vodka, we get it.
                </ScrollItem>
                <ScrollItem as={'div'} itemStyle={{fontSize:72 ,top:`${70}%`,left:`${50+textMore}%`,color:theme.palette.text.primary}}
                                         start={roll2Start} end={0}
                                         inMethod={ANIITEM_CONST.getValidMethod(ANIITEM_CONST.METHOD.ROLL,ANIITEM_CONST.DIRECTION.UP)}>
                And <b>MORE</b>
            </ScrollItem>
            </Section>
        )
    };

    renderFinalSection=()=>{//TODO refactor calculation to outside
        const {theme} = this.props;

        let bgHeight = 60;
        let bgHor = (100-bgHeight*fodBgRatio/aspectRatio)/2;



        return(
            <Section contentHeight={0}>

                <ScrollItem as={'div'} itemStyle={{background:`url(${bgIcon}) no-repeat center`, backgroundSize:'contain',
                    top:`${30}%`,bottom:`${10}%`,right:`${bgHor}%`,left:`${bgHor}%`}}
                            start={0} end={0}
                />
                <ScrollItem as={'div'} itemStyle={{fontSize:72, fontWeight:'bold' ,top:`${5}%`,left:`${0}%`,right:`${0}%`,color:theme.palette.text.secondary,textAlign:'center'}}
                            start={0} end={0}>
                    F.O.D OnDemand
                </ScrollItem>
                <ScrollItem as={'div'} itemStyle={{fontSize:64, fontWeight:'bold' ,top:`${18}%`,left:`${0}%`,right:`${0}%`,color:theme.palette.text.primary,textAlign:'center'}}
                            start={0} end={0}>
                    Delivery and PickUp Platform
                </ScrollItem>

                <ScrollItem as={'div'} itemStyle={{fontSize:18,lineHeight:'1em',top:`${30}%`,bottom:`${10}%`,left:`${15}%`,right:`${15}%`,color:theme.palette.text.primary,textAlign:'left',display:'flex',alignItems:'center'}}
                            start={0} end={0}>
                    Able to deliver anything from anywhere, Postmates is the food delivery, grocery delivery, whatever-you-can-think-of delivery service to bring what you crave right to your door. We’re the largest in the Universe with more than 25,000+ partner merchants, many of them exclusive, and we’re adding more every day. Every customer enjoys a curated and tailored experience, showcasing the very best of their city. Just enter your address, find something you like, and add it to your cart. Once you place your order we’ll forward your payment to the store and you can watch us zigzag through the city streets to bring your package to you.
                </ScrollItem>

                <ScrollItem as={'div'} itemStyle={{fontSize:16 ,top:`${95}%`,left:`${15}%`,right:`${15}%`,color:theme.palette.text.primary,textAlign:'left',display:'flex',flexDirection:'row'}}
                            start={0} end={0}>
                    <span style={{flex:2}}>Copyright @ 2019 F.O.D. All Rights Reserved.</span>
                    <TandC onClick={()=>this.setState({drawerOpen:true})} style={{flex:1}}>Terms & Conditions</TandC>
                    <span style={{flex:1}}>Contact: 289-597-3784</span>
                    <span style={{flex:1}}>Email: info@myfod.ca</span>
                </ScrollItem>

            </Section>
        )
    };

    renderDrawer=()=>{
        const {theme} = this.props;
        const {drawerOpen} = this.state;
        return(
            <Drawer
                ModalProps={{
                BackdropProps:{style:{backgroundColor:'transparent'}}
            }}
                    PaperProps={{
                        style:{backgroundColor:'transparent'}
                    }}
                    anchor="bottom" open={drawerOpen} onClose={()=>this.setState({drawerOpen:false})}>
                <div style={{width:'100vw',height:'100vh',display:'flex',flexDirection:'row'}}>
                    <div style={{width:theme.spacing.unit*(10+10)+159}} onClick={()=>this.setState({drawerOpen:false})}>
                        <DrawerSide>
                            <DrawerSideItem onClick={(event)=>event.stopPropagation()}>
                                Terms And Conditions
                            </DrawerSideItem>
                        </DrawerSide>
                    </div>
                    <div style={{flex:1,backgroundColor:'white',zIndex:100,boxShadow: "-2px 0 6px 1px rgba(0,0,0,0.50)"}} >
                        {this.renderTAndC()}
                        <HighlightOffIcon style={{position:'fixed', top:theme.spacing.unit*6,right:theme.spacing.unit*6,color:theme.palette.text.secondary,cursor:'pointer'}} fontSize={'large'} onClick={()=>this.setState({drawerOpen:false})}/>
                    </div>
                </div>
            </Drawer>
        )
    };

    renderTAndC=()=>{
        const {theme} = this.props;
        return(
            <div style={{paddingTop:theme.dim.navHeight,paddingBottom:theme.dim.navHeight, paddingLeft:theme.spacing.unit*8,paddingRight:theme.spacing.unit*8,overflowY:'auto',height:'100%',boxSizing:'border-box'}}>
                <p style={{fontSize:24, fontWeight:'bold', color:theme.palette.text.primary,marginBottom:40,marginTop:0}}>Terms & Condition</p>
                <div style={{textAlign: 'justify',lineHeight:"1em",fontSize:16, color:theme.palette.text.primary}}>
                    The following are the website and mobile application's terms of use for 9595228 CANADA INC. (o/a <b>"F.O.D"</b>). By accessing <a rel="noopener noreferrer" href="https://www.myfod.ca/" target="_blank">https://www.myfod.ca/</a> or any F.O.D. mobile applications, you acknowledge these terms of use and agree to be bound by them. If you do not agree to these terms of use, please do not access this site. You acknowledge and agree that the services of F.O.D. is a private service website/mobile application owned and operated by 9595228 CANADA INC.
                    <br/>
                    <br/>
                    If you are accessing or using F.O.D. on behalf of a business, you authorize that business to agree to the Terms of Use or you shall seek such proper authorization without which you agree not to use the website, mobile application, and/or services (the “<b>Services</b>”) provided by F.O.D. Without limiting the foregoing, If you do not agree to anything within this Terms of Use, you are not authorized to use the Services.
                    <br/>
                    <br/>
                    We reserve the right, in our sole discretion, to modify, alter or otherwise update these terms of use at any time and you agree to be bound by such modifications, alterations or updates. The terms <b>"FOD"</b> and <b>"we"</b> refer to 9595228 CANADA INC as applicable. Our privacy policy and all other policies, rules and agreement, if any, referenced below are fully incorporated into this Terms of Use and you agree to them as well.
                    <br/>
                    <br/>
                    THE COMPANY DOES NOT PROVIDE TRANSPORTATION SERVICES AND THE COMPANY IS NOT A TRANSPORTATION CARRIER. IT IS UP TO THE THIRD PARTY TRANSPORTATION PROVIDER, BICYCLE, DRIVER OR VEHICLE OPERATOR TO OFFER TRANSPORTATION SERVICES WHICH MAY BE SCHEDULED THROUGH USE OF THE APPLICATION OR SERVICE. THE COMPANY OFFERS INFORMATION AND A METHOD TO OBTAIN SUCH THIRD PARTY TRANSPORTATION SERVICES. THE COMPANY OFFERS INFORMATION AND A METHOD TO OBTAIN SUCH THIRD PARTY TRANSPORTATION SERVICES, BUT DOES NOT AND DOES NOT INTEND TO PROVIDE TRANSPORTATION SERVICES OR ACT IN ANY WAY AS A TRANSPORTATION CARRIER, AND HAS NO RESPONSIBILITY OR LIABILITY FOR ANY TRANSPORTATION SERVICES PROVIDED TO YOU BY SUCH THIRD PARTIES.
                    <br/>
                    <ul>
                        <li>
                            <b><u>User Obligations</u></b>: without limiting any other rights of F.O.D. hereunder, the user of the F.O.D. Services:
                            <br/>
                            <ul>
                                <li>agree that the user is of sufficient age and capacity to use the Services within your jurisdiction;</li>
                                <li>agree to provide F.O.D. with accurate personal information, including but not limited to any information necessary to setup any accounts;</li>
                                <li>agree not to violate any applicable law;</li>
                                <li>agree not to buy/sell accounts;</li>
                                <li>agree not to setup multiple accounts;</li>
                                <li>agree not to do or omit to do anything which would infringe on the rights of a third-party;</li>
                                <li>agree not to copy, modify, or redistribute any other person’s content;</li>
                                <li>agree to be bound by all policies and rules made by F.O.D. with respect to the Services from time to time;</li>
                                <li>agree that F.O.D. may moderate access/use of the Services in its sole discretion, e.g., by blocking, filtering, re-categorizing, re-ranking, deleting, delaying, holding, omitting, verifying, or terminating your access/account, and that the user accepts such moderation and would not obstruct such moderation, and that F.O.D. is not liable for anything which may result from such moderation;</li>
                                <li>agree not to use or provide software (except general purpose web browsers and email clients) or services that interact or interoperate with F.O.D. , e.g. for downloading, uploading, creating/accessing/using an account, posting, flagging, emailing, searching, or mobile use unless expressly authorized by F.O.D;</li>
                                <li>not to disrupt or interfere with the security of, or otherwise abuse, the website, or any services, system resources, accounts, servers or networks or linked websites in relation to thereof;</li>
                                <li>not to disrupt or interfere with any other party’s enjoyment of the website linked websites or mobile application;</li>
                                <li>not to upload, post, spread, or otherwise transmit through or on the website or mobile application any viruses or other harmful, disruptive or destructive files;</li>
                                <li>not to attempt to obtain unauthorized access to the website or portions of the website which are restricted from access made available to the user by F.O.D.</li>
                                <li>to produce IDs are required by law such as where alcohol and/or tobacco has been ordered through the Services.</li>
                                <li>to take such reasonable action as required by F.O.D. for the completion of any transactions conducted through the Services of F.O.D., including but not limited to: the signing of any and all receipts for deliveries where required by law.</li>
                            </ul><br/></li>
                        <li><b><u>Service Fees</u></b>: The user authorize us to charge the user’s account for F.O.D. fees as determined and posted by F.O.D. in its sole discretion from time to time. Unless otherwise provided for by the policies of F.O.D., Fees are non-refundable, even for any transactions that were cancelled, delayed, failed, or moderated. Fees are due immediately unless expressly stated otherwise. If your payment method fails or your account is past due, we may collect fees owed using other collection mechanisms (for accounts over 180 days past due – such time limit subject to change at the discretion of F.O.D. you permit us to instruct any applicable online financial service providers to deduct the amount owed from your financial service providers account balance).
                            <br/>
                            <br/>
                            All fees posted are in the currency of your jurisdiction unless otherwise posted or otherwise specific negotiated between yourself and any other users of the Services.
                            <br/>
                            <br/>
                            The user shall be responsible for the collection of any and all taxes for which he she or it may be liable under the law of the applicable jurisdiction. F.O.D. will only collect and be responsible for taxes which it is specifically obligated to collect under the laws of its governing jurisdiction.
                            <br/>
                            <br/>
                            The user acknowledges where alcohol and/or tobacco is ordered through the Services additional charges as required by law may apply and where the user does not comply with law in its receipt of these deliveries, additional conditions for refunds or other processing of payments or the items delivered may apply.<br/><br/></li>

                        <li><b><u>Account Information</u></b>: In order to use the Services, you will be required to register and set up an account with your personal information and a password (your <b>"Account"</b>). You are solely responsible for maintaining the confidentiality of your password. You are solely responsible for all activities that occur under your Account. If you believe your Account may have been compromised or misused, contact us immediately.<br/><br/></li>

                        <li><b><u>Copyright</u></b>: Information on this site, including but not limited to text, graphics, images, downloadable content, and links to other Internet resources, (the <b>"Site Information"</b>) is protected under the copyright laws of Canada and its provinces where applicable. Unless otherwise specified, the Site Information may not be copied, displayed, communicated, distributed, reproduced or republished, in any form, without advance express written consent.<br/><br/></li>

                        <li><b><u>Trademarks</u></b>: If any trademarks and logos (collectively, <b>"Marks"</b>) are displayed on the website are registered or unregistered Marks which are the property of F.O.D. or other third parties (where such Marks are displayed). The display of these Marks on the website does not imply that a license or right of any kind has been granted. You are not permitted to use or display any of these Marks without the prior written consent of F.O.D. or the applicable third party. Nothing in the Website is to be interpreted as conferring a right to use the Marks or the material protected by the applicable provincial or federal legislation.<br/><br/></li>

                        <li><b><u>Information Non-warranty and F.O.D. Non-Liability</u></b>: This WEBsite, MOBILE APPLICATION and the Information/COMMUNICATIONS provided THEREIN EITHER BY F.O.D. OR BY ANY OTHER USER THROUGH F.O.D. shall be received by the user “as is”. F.O.D. doES not warrant the accuracy, adequacy, timeliness, SAFETY, LEGALITY, FITNESS OR SUITABILITY FOR A PARTICULAR PURPOSE, MERCHANTABILITY WHERE APPLICABLE, or completeness of SUCH Information/COMMUNICATIONS, and disclaim liability for:<br/><br/>
                            <ul>
                                <li>ALL SUCH INFORMATION PROVIDED BY F.O.D. ON THE WEBSITE AND MOBILE APPLICATION; AND</li>
                                <li>ALL SUCH INFORMATION/COMMUNICATIONS PROVIDED BY ANY OTHER USERS OF THE F.O.D. WEBSITE OR MOBILE APPLICATION WHETHER AS SUCH USERS ARE CUSTOMERS OR FOOD/cATERING SERVICE PROVIDERS SUCH AS RESTAURANTS.</li>
                                <li>ANY SUBSEQUENT ACTIONS OF ANY USER WHICH IS A CONSEQUENCE OF THEM USING THE SERVICES, OCCURRING EITHER DURING THE DELIVERY PROCESS OR OTHERWISE, INCLUDING BUT NOT LIMITED TO, CAUSING PERSONAL INJURY, OR DEATH TO ANOTHER USER, CAUSING DAMAGE TO ANY ITEMS TO BE TRANSPORTED, OR CAUSING A FINANCIAL LOSS TO ANY OTHER USER EITHER DIRECT OR INDIRECT, OR ANY OTHER ACTIONS WHICH MAY GIVE RISE TO LIABILITY AT LAW OR IS A VIOLATION OF A LAW OF THE APPLICABLE JURISDICTION.</li>
                            </ul>
                            <br/>
                            No Warranty or condition of any kind (implied, express, or statutory) is given in the WEBsite or MOBILE APPLICATION OR ANY INFORMATION/COMMUNICATIONS PROVIDED BY ANY OTHER USERS, INCLUDING ANY CUSTOMERS OR FOOD/cATERING SERVICE PROVIDERS SUCH AS RESTAURANTS WHO USED the WEBSITE OR MOBILE APPLICATION OF F.O.D.
                            <br/><br/>
                            F.O.D. WILL NOT BE LIABLE FOR ANY DAMAGES, LOSSES OR EXPENSES (INCLUDING DIRECT, INDIRECT, SPECIAL, INCIDENTAL, CONSEQUENTIAL OR PUNITIVE DAMAGES, INCLUDING BUT NOT LIMITED TO LOST PROFITS, REVENUES, DATA OR GOODWILL) ARISING FROM THE USE OR INABILITY TO USE ITS WEBSITE, MOBILE APPLICATION OR THE SERVICES OR THE INFORMATION/COMMUNICATIONS PROVIDED BY ANY OTHER USERS, OR AN ERROR, OMISSION, INTERRUPTION, DEFECT, DELAY, COMPUTER VIRUS, SYSTEM FAILURE, LOSS OF DATA OR OTHERWISE, EVEN IF F.O.D. OR ITS PERSONNEL ARE ADVISED OF THE POSSIBILITY OF SUCH DAMAGES, LOSSES OF EXPENSES.
                            <br/><br/>
                            For greater clarity, the F.O.D. does not disclaim any liability on behalf, or disclaim any warranty on behalf of, any user of its Services, where such user is providing information to another user (for example, as a restaurant to a customer).
                            <br/><br/>
                            For greater clarity, where applicable, the user acknowledges that even though F.O.D. endeavors to provide accurate tracking information for any items through its system, such information may be provided by third-party services and such accuracy is not guaranteed by F.O.D. and should not be relied upon.
                            <br/><br/>
                            The user also agree not to hold F.O.D. responsible for the payment processing of other service providers such as Visa or MasterCard (<b>"Third Party Payment Processor"</b>). The user acknowledge and agree that by making payments through F.O.D. with a Third Party Payment Processor, the User is bound by that Third Party Payment Processor’s applicable terms and conditions, and F.O.D. is not liable for any loss, claims or damages howsoever arising in connection with that third party’s services).
                            <br/><br/>
                            Despite the previous paragraphs, if F.O.D. is found to be liable, the liability to the user or any third party (whether in contract, tort, negligence, strict liability in tort, by statute or otherwise) is limited to the greater of (a) the total fees the user has pay to F.O.D. in the 12 months prior to the action giving rise to liability, and (b) 100 Canadian Dollars.
                            <br/><br/></li>
                        <li><b><u>Hyperlinks & Non-F.O.D. Websites</u></b>: Hyperlinks to other websites or internet mechanisms are to be used at your own risk. F.O.D. makes no representations or warranties whatsoever about other websites which you may access through its website or mobile application. Non-F.O.D. are not subject to F.O.D.’s privacy policy or other applicable security standards. Links to non-F.O.D. web sites do not mean F.O.D. endorses or accepts any responsibility for such websites or their content.<br/><br/></li>

                        <li><b><u>Indemnification</u></b>: THE USER AGREE THAT THE USER’S USE OF THE WEBSITE/MOBILE APPLICATION AND/OR THE CONTENTS THEREIN, SHALL NOT BE MADE THE BASIS FOR ANY CLAIM, SUIT, DEMAND OR CAUSE OF ACTION OR OTHER PROCEEDING AGAINST F.O.D., ITS OFFICERS, DIRECTORS, EMPLOYEES OR OTHER AGENTS. THE USER AGREE, AT THE USER’S OWN EXPENSE, TO INDEMNIFY, DEFEND AND HOLD F.O.D., ITS OFFICERS, DIRECTORS, EMPLOYEES OR AGENTS HARMLESS AGAINST ANY CLAIM, SUIT, DEMAND, ACTION OR OTHER PROCEEDING BROUGHT OR THREATENED BY A THIRD PARTY AS A RESULT OF (I) THE USER’S BREACH OF THESE TERMS; AND/OR (II) THE USER”S USE OF THE WEBSITE/MOBILE APPLICATION AND/OR THE CONTENTS THERE IN AND PAY FOR ALL LIABILITY, EXPENSES, LEGAL COSTS, AND OTHER COSTS AS A RESULT THEREOF.
                            <br/><br/>
                            F.O.D. reserve the right, at its own expense, to assume the exclusive defense and control of any matter subject to indemnification by the user, but doing so will not excuse the user’s indemnity obligations.
                            <br/><br/>
                            This indemnity shall survive any termination of use of the Services by the user.
                            <br/><br/></li>
                        <li><b><u>Privacy</u></b>: You have read the Privacy Policy, the terms of which can be found on the F.O.D. website and are incorporated into these Terms by reference, and agree that the terms of such policy are reasonable. F.O.D. values and respects your privacy, and is committed to protecting your personal information by keeping it confidential and secure. F.O.D.’s collection and retention of your personal information is governed by the terms of the Personal Information Protection and Electronic Documents Act, the Canadian Anti-Spam Legislation and other relevant legislation.<br/><br/></li>

                        <li><b><u>Cookies</u></b>: When you visit our websites, a small data file called a cookie is sent to your computer. Cookies are generally used to make it more convenient to use our websites. Persistent cookies are stored on your computer when you visit certain websites, while session cookies expire when you close your browser. F.O.D. may use either or both types of cookies. Session cookies facilitate your use and navigation of our sites, while persistent cookies identify your browser and IP (Internet Protocol) address, but do not collect personal information such as your name and address.<br/><br/></li>

                        <li><b><u>Security</u></b>: Without limiting any other rights of F.O.D. hereunder, the user acknowledges that no method of transmission over the Internet, or method of electronic storage, is 100 percent secure. While F.O.D. strive to use commercially acceptable means to protect any user information but cannot guarantee its absolute security.<br/><br/></li>

                        <li><b><u>Mobile Devices</u></b>: If the user accessing the Services from a mobile device using a mobile application developed by F.O.D. (the <b>"App"</b>), the following terms and conditions apply to the user in addition to the applicable terms of this Terms of Use:
                            <br/><br/>
                            <ul>
                                <li>F.O.D. grants the user the right to use the App only for the user’s personal use. The user must comply with all applicable laws and third party terms of agreement when using the App (e.g. any wireless data service agreement). The App may not contain the same functionality available on any websites operated by F.O.D.  The User’s download and use of the App is at the user’s own discretion and risk, and the user is solely responsible for any damages to the user’s hardware device(s) or loss of data that results from the download or use of the App.</li>
                                <li>F.O.D. owns, or is the licensee to, all right, title, and interest in and to its App, including all rights under patent, copyright, trade secret, trademark, and any and all other proprietary rights, including all applications, renewals, extensions, and restorations thereof. The user will not modify, adapt, translate, prepare derivative works from, decompile, reverse-engineer, disassemble, or otherwise attempt to derive source code from any App and you will not remove, obscure, or alter F.O.D.’s copyright notice, trademarks or other proprietary rights notices affixed to, contained within, or accessed in conjunction with or by any App.</li>
                                <li>These Terms of Use are an agreement between the user and F.O.D., and not with the user’s mobile device carrier or the manufacturer of the user’s mobile device (<b>"Mobile Company"</b>). The Mobile Company is not responsible for the App and the content thereof.</li>
                                <li>F.O.D. grants the user the right to use the App only on mobile device that the user owns or controls and where it is permitted by the Mobile Company, which the responsibility to confirm shall be on the user.</li>
                            </ul>
                            <br/></li>
                        <li><b><u>Non-Competition</u></b>: The user agrees that during the term of the use of the Services by the user, and during the term when the user’s account with F.O.D. is not terminated, the user shall not, for any reason, directly or indirectly, whether as an employee for as a business owner, in any capacity compete with the business of F.O.D. and for a period of twelve (12) months after the termination of the user of such Services or the account of the user whichever is longer.<br/><br/></li>

                        <li><b><u>Applicable Law</u></b>: Except where prohibited by the laws of your jurisdiction, this website will be governed by and construed in accordance with the laws of the Province of Ontario and the federal laws of Canada as applicable therein, without regard to the principles of conflicts of law.<br/><br/></li>

                        <li><b><u>Non-Waiver</u></b>: failure by F.O.D. to insist upon strict adherence to any term of this Terms of Use on any occasion shall not be considered a waiver of F.O.D.’s rights or deprive such it of the right thereafter to insist upon strict adherence to that term or any other term of this Terms of Use.<br/><br/></li>

                        <li><b><u>Severability</u></b>: In the event any provision in this Terms of Use shall be held invalid, illegal or unenforceable, the validity, legality and enforceability of the remaining provisions shall not in any way be affected or impaired thereby.<br/><br/></li>

                        <li><b><u>Lawful Use</u></b>: In addition to complying with these terms, you agree to use the website and the contents therein for lawful purposes only and in a manner consistent with local, national or international laws and regulations. Some jurisdictions may have restrictions on the use of the Internet by their residents.<br/><br/></li>

                        <li><b><u>Survival</u></b>: without limiting any other rights of F.O.D. hereunder, as reasonably interpreted the applicable clauses of this Terms of Use shall survive any termination of use of the Services by the user.<br/><br/></li>
                    </ul>
                </div>

            </div>
        )
    };

    render(){
        const {theme}=this.props;
        return(
            <div>
                <ScrollFrame minHeight={780} aspectRatio={aspectRatio} topSkip={theme.dim.navHeight}>
                    {this.renderiOSSection()}
                    {this.renderAndroidSection()}
                    {this.renderDetailSection()}
                    {this.renderFinalSection()}
                </ScrollFrame>
                {this.renderDrawer()}}
            </div>
        )
    }
}

const mapDispatchToProps = dispatch => {
    return {
        openAndroidModal: () => {
            dispatch(toggleAndroidModal(true));
        },
        openIOSModal: () => {
            dispatch(toggleIOSModal(true));
        },
    }
};
export default connect(null, mapDispatchToProps)(withTheme(Scroller));

