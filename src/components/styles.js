
export const styledTheme={
    palette:{
        main:'rgb(10,125,180)',
        text:{
            primary:'rgb(74,74,74)',
            secondary:'rgb(155,155,155)',
        },
    },
    spacing:{
        unit:8,
    },
    dim:{
        navHeight:170,
        //width:1280, //780 16:9.75
        //height:780
    }
};