
import React, {Component} from 'react'
import styled from 'styled-components'

/*
ScrollFrame
props:
skipTop(opt)px:for top occupied component
minHeight(req)px:minHeight for the show section, auto scale up to fit screen
aspectRatio(req)dec:for calculate width

children: list of Section
 */


/*
Section
props:
contentHeight(req) px: scroll height for this section

from Frame:
scroll px: total scrollOffset
canvasWidth/Height px: width height for section canvas, calculated in Frame
start px: start of the section respect to total scroll

children: list of ScrollItem
 */

/*
ScrollItem
props:
start: when to TOTALLY show the item respect to current scrollOffset
end: when to START REMOVE the item respect to current scrollOffset
in/outMethod(opt) obj: specify behaviour of item move in/out of the section, default: nothing, just pop up //TODO:test opt of in/outMethod
as(opt) tag: the component wish to be used for the item
itemStyle (req) styleObj: style apply to component, including position information ...etc

from Section:
scrollOffset px: offset respect to current Section
contentHeight: as above, for supporting percentage input //TODO:test
 */

/*
ANIITEM OBJ
method(req): ROLL, FADE ; open to more method
direction(opt): respect to ROLL, the direction of roll in/out
actLength(opt) px:  length of scroll for item to move in/out
displacement(opt) px: length for ROLL to move in/out on display
 */

//TODO:remove test

const ScrollBase = styled.div`
  height:${props=>props.height}px;
`;

export class ScrollFrame extends Component {
    constructor(props) {
        super(props);
        this.state = {
            numSec:0,
            baseTop:0,
            totalContentHeight:0,
        };
        this.state.canvasHeight = Math.max(props.minHeight,window.innerHeight-props.topSkip);
        this.state.minCurHeightRatio=(this.props.minHeight===this.state.canvasHeight? 1:(this.state.canvasHeight/this.props.minHeight));

            this.state.canvasWidth = this.state.canvasHeight*props.aspectRatio;
        this.state.totalHeight = 0;
        window.addEventListener("resize", this.onResize);
    }

    componentWillUnmount() {
        window.removeEventListener('scroll', this.onScroll);
    }

    componentDidMount(){
        let totalHeight=0;
        let totalSection=0;
        React.Children.forEach(this.container.props.children,child=>{
            totalSection+=1;
            totalHeight+=child.props.contentHeight;
        });
        this.setState({totalContentHeight:totalHeight,numSec:totalSection},this.onResize);
        window.addEventListener('scroll', this.onScroll);
    }

    onResize=()=>{
        let canvasHeight = Math.max(this.props.minHeight,window.innerHeight-this.props.topSkip);
        this.setState({
            totalHeight:Math.max((this.state.numSec-1),0) * canvasHeight + this.props.topSkip + this.state.totalContentHeight+ canvasHeight,
            /* switch section space + top space + total content space + canvas itself*/
            canvasHeight,
            canvasWidth:canvasHeight*this.props.aspectRatio,
            minCurHeightRatio:(this.props.minHeight===canvasHeight? 1:(canvasHeight/this.props.minHeight)),

        });

    };
    onScroll=()=>{
        this.setState({baseTop:document.documentElement.scrollTop})
    };

    render(){
        const {canvasHeight,canvasWidth,baseTop,minCurHeightRatio} = this.state;
        //console.log(minCurHeightRatio);
        //console.log(this.state);
        //console.log(this.state.baseTop);
        return (
            <ScrollBase height={this.state.totalHeight} >
                <ScrollContainer ref={ref=>this.container=ref} minCurHeightRatio={minCurHeightRatio} scroll={baseTop} canvasHeight={canvasHeight} canvasWidth={canvasWidth} topSkip={this.props.topSkip}>
                    {this.props.children}
                </ScrollContainer>
            </ScrollBase>
        )
    }
}

//==========Scroll==================

const ScrollContain = styled.div`
    position:fixed;
    height:${props=>props.canvasHeight}px;
    width:${props=>props.canvasWidth}px;
    top:${props=>props.topSkip}px;
    left:${props=> (window.innerWidth-props.canvasWidth)/2}px;
    margin:0 auto;
`;

class ScrollContainer extends Component {
    render(){
        const {scroll,canvasWidth,canvasHeight,minCurHeightRatio} = this.props;
        let start = 0;
        let nextStart = 0;
        const children = React.Children.map(this.props.children, child => {
            start = nextStart; //auto calculate content height
            nextStart+=(((child.props&&child.props.contentHeight)?child.props.contentHeight:0)+canvasHeight);
            //console.log(start);
            //console.log(nextStart);
            if ((scroll< start - canvasHeight - 200 )|| scroll > nextStart + canvasHeight+200){
                return null
            }
            return React.cloneElement(child, {
                scroll,
                canvasWidth,
                canvasHeight,
                start,
                minCurHeightRatio,
            });
        });
        //style={{backgroundColor:'blue'}}
        return(
            <ScrollContain  canvasHeight={this.props.canvasHeight} canvasWidth={this.props.canvasWidth} topSkip={this.props.topSkip}>
                {children}
            </ScrollContain>
        )
    }

}

//=============Section==================
const SectionBase = styled.div`
    position:absolute;
    height:${props=>props.canvasHeight}px;
    width:${props=>props.canvasWidth}px;
    transform:translate(0,${props=>props.translateY}px);
    opacity:${props=>(props.translateY>=0?1:1+props.translateY/props.canvasHeight)};
`;

export class Section extends Component {
    render(){
        const {start,contentHeight,canvasHeight,canvasWidth,scroll,minCurHeightRatio} = this.props;
        const end = start+contentHeight;
        if(scroll>=end+canvasHeight || scroll<= start-canvasHeight){
            return null
        }
        let scrollOffset = scroll - start;
        const children = React.Children.map(this.props.children, child => {
            return React.cloneElement(child, {
                //percentage,
                scrollOffset,
                contentHeight,
                minCurHeightRatio,
            });
        });

        let translateY = 0;
        if(start-canvasHeight<scroll && scroll< start){
            translateY = start-scroll;
        }else if (end<scroll && scroll < end+canvasHeight){
            translateY = end - scroll;
        }
        return(
            <SectionBase  translateY={translateY} canvasHeight={canvasHeight} canvasWidth={canvasWidth} style={{backgroundColor:this.props.test}}>
                {children}
            </SectionBase>
        )
    }
}

//=============Item=============
export class ANIITEM_CONST {
    static METHOD = {
        FADE: "fade",
        ROLL: "roll",//+fade automatically
        SUDDEN: "SUDDEN"
    };

    static DIRECTION = {
        UP:"up",
        DOWN:"down",
        LEFT:"left",
        RIGHT:"right",
    };

    static isValidMethodObject(obj){
        return obj&&this.isValidMethod(obj.method,obj.direction)&&((!obj.actLength)||typeof(obj.actLength)==='number')&&((!obj.displacement)||typeof(obj.displacement)==='number');
    }

    static isValidMethod(method,direction){
        return method&&(method===this.METHOD.FADE||method===this.METHOD.SUDDEN||(method===this.METHOD.ROLL&&this.isValidDirection(direction)));
    }

    static isValidDirection(direction){
        return direction&&(direction===this.DIRECTION.UP||direction===this.DIRECTION.DOWN||direction===this.DIRECTION.LEFT||direction===this.DIRECTION.RIGHT);
    }

    static getValidMethod(method,direction,actLength,displacement){
        let ret = {
            method,
            actLength,
            direction,
            displacement,
        };
        //console.log(ret);
        if (!this.isValidMethodObject(ret)) {
            console.warn('method not valid');
            console.warn(ret);
        }
        return this.isValidMethodObject(ret)?ret:{};

    }
}


const AniItem = styled.div`
    position:absolute;
    transform: translate(${props=>props.translateX}px,${props=>props.translateY}px);
    opacity:${props=>props.opacity};
`;
export class ScrollItem extends Component{
    constructor(props){
        super(props);
        this.defaultActionLength = 200;
        this.defaultDisplacement = 200;
    }

    getResponsiveTextSize=(size)=>{
        return size*this.props.minCurHeightRatio;
    };

    render(){
        const {start,end, inMethod, outMethod, itemStyle, scrollOffset, contentHeight, children, ...rest} = this.props;
        //console.log(inMethod);
        //console.log("=====================");
        //console.log(scrollOffset);
        //console.log(contentHeight);

        let startPixel = start?(start<1?start*contentHeight:start):0; //percentage or pixel
        let isInMethodSet = ANIITEM_CONST.isValidMethodObject(inMethod);
        let startActLength = isInMethodSet?(inMethod.actLength?(inMethod.actLength<1?inMethod.actLength*contentHeight:inMethod.actLength):this.defaultActionLength):0;//allow percentage as 0~1
        let startActionPixel = isInMethodSet?(inMethod.method===ANIITEM_CONST.METHOD.SUDDEN?startPixel:(startPixel-startActLength)):0;
        let startDisplacement = isInMethodSet?(inMethod.displacement?inMethod.displacement:this.defaultDisplacement):0;

        //console.log("-----------------");
        //console.log(startPixel);
        //console.log(startActLength);
        //console.log(startActionPixel);
        //console.log(startDisplacement);

        let endPixel = end?(end<1?end*contentHeight:end):contentHeight;
        let isOutMethodSet = ANIITEM_CONST.isValidMethodObject(outMethod);
        let endActLength = isOutMethodSet?(outMethod.actLength?(outMethod.actLength<1?outMethod.actLength*contentHeight:outMethod.actLength):this.defaultActionLength):0;//allow percentage as 0~1
        let endActionPixel = isOutMethodSet?(outMethod.method===ANIITEM_CONST.METHOD.SUDDEN?endPixel:(endPixel+endActLength)):contentHeight;
        let endDisplacement = isOutMethodSet?(outMethod.displacement?outMethod.displacement:this.defaultDisplacement):0;

        //console.log("----------------");
        //console.log(endPixel);
        //console.log(endActLength);
        //console.log(endActionPixel);
        //console.log(endDisplacement);

        let opacity = 0;
        let translateX = 0;
        let translateY = 0;
        // ------- startActionPixel -------------- startPixel ------- endPixel ------------ endActionPixel --------
        // ------------------------ startActLength --------------------------- endActLength -----------------------

        if((startPixel<= scrollOffset) && (scrollOffset <= endPixel)){//include // within
            //console.log("within");
            opacity=1;
        }else if(scrollOffset <= startActionPixel ){//include //total before in
            //console.log("total before in");
            opacity= isInMethodSet?0:1;
        }else if (scrollOffset >= endActionPixel){ //include //total after out
            //console.log("total after out");
            opacity= isOutMethodSet?0:1;
        }else if(scrollOffset > startActionPixel && scrollOffset <startPixel){ //exclude //during in
            //console.log("during in");
            opacity = (scrollOffset - startActionPixel)/startActLength;
            if(isInMethodSet&&inMethod.method===ANIITEM_CONST.METHOD.ROLL){
                switch (inMethod.direction) {
                    case ANIITEM_CONST.DIRECTION.DOWN:
                        translateY= (-startDisplacement*(1-opacity));
                        break;
                    case ANIITEM_CONST.DIRECTION.RIGHT:
                        translateX= (-startDisplacement*(1-opacity));
                        break;
                    case ANIITEM_CONST.DIRECTION.LEFT:
                        translateX= startDisplacement*(1-opacity);
                        break;
                    case ANIITEM_CONST.DIRECTION.UP:
                    default:
                        translateY= startDisplacement*(1-opacity);
                        break;
                }
            }
        }else if(scrollOffset < endActionPixel && scrollOffset >endPixel){//exclude //during out
            //console.log("during out");
            opacity = (1- (scrollOffset - endPixel)/endActLength);
            if(isOutMethodSet&&outMethod.method===ANIITEM_CONST.METHOD.ROLL){
                switch (outMethod.direction) {
                    case ANIITEM_CONST.DIRECTION.DOWN:
                        translateY= endDisplacement*(1-opacity);
                        break;
                    case ANIITEM_CONST.DIRECTION.RIGHT:
                        translateX= endDisplacement*(1-opacity);
                        break;
                    case ANIITEM_CONST.DIRECTION.LEFT:
                        translateX= (-endDisplacement*(1-opacity));
                        break;
                    case ANIITEM_CONST.DIRECTION.UP:
                    default:
                        translateY= (-endDisplacement*(1-opacity));
                        break;
                }
            }
        }

        //console.log("----------------");
        //console.log(opacity);
        //console.log(translateX);
        //console.log(translateY);
        let styles = Object.assign({},itemStyle);
        if(styles.fontSize){
            styles.fontSize = this.getResponsiveTextSize(itemStyle.fontSize);
        }

        return (
            <AniItem style={styles} translateY={translateY} translateX={translateX} opacity={opacity} {...rest}>
                {children}
            </AniItem>
        )
    }
}