
import * as modalActions from '../actions/modalActions'

const initialState = {
    androidOpen:false,
    iOSOpen:false,
};


export default function modal(state=initialState, action) {
    switch (action.type) {
        case modalActions.TOGGLE_ANDROID_MODAL:
            state.androidOpen = action.isOpen;
            return Object.assign({},state);
        case modalActions.TOGGLE_IOS_MODAL:
            state.iOSOpen = action.isOpen;
            return Object.assign({},state);
        default:
            return state;
    }
}