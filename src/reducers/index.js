import { combineReducers } from 'redux'
import modal from './modalReducer'

const reducer = combineReducers({
    modal
});

export default reducer